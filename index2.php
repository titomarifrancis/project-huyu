<?php
   $response='{
  "id": "10204507755988171",
  "picture": {
    "data": {
      "is_silhouette": false,
      "url": "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p50x50/11701198_10206044204558425_4857807799857647785_n.jpg?oh=d0e57e6f80204f4119e9fc2300ab5033&oe=584AB010&__gda__=1480909845_f1b1186add06f115696e4c1e59b37d1d"
    }
  },
  "email": "titomarifrancis@gmail.com",
  "gender": "male",
  "cover": {
    "id": "10203834790964466",
    "offset_y": 66,
    "source": "https://fbcdn-photos-a-a.akamaihd.net/hphotos-ak-xtp1/t31.0-0/p180x540/10712624_10203834790964466_4709062922133585564_o.jpg"
  },
  "birthday": "10/04/1975",
  "location": {
    "id": "102182963156749",
    "name": "Taguig, Philippines"
  },
  "relationship_status": "Married",
  "religion": "Roman Catholic",
  "work": [
    {
      "employer": {
        "id": "257425257648668",
        "name": "EDGEKIT Computer Systems"
      },
      "location": {
        "id": "102182963156749",
        "name": "Taguig, Philippines"
      },
      "position": {
        "id": "125673630810689",
        "name": "System Solutions Architect"
      },
      "start_date": "2014-12-31",
      "id": "10208140709529739"
    },
    {
      "end_date": "2016-03-31",
      "employer": {
        "id": "23802140373",
        "name": "Accenture"
      },
      "location": {
        "id": "106050279435951",
        "name": "Quezon City, Philippines"
      },
      "position": {
        "id": "139479519406623",
        "name": "Software Engineering Team Lead"
      },
      "start_date": "2015-01-01",
      "id": "10205173557792800"
    },
    {
      "end_date": "2013-01-01",
      "employer": {
        "id": "108134942553157",
        "name": "Aquinas University of Legazpi"
      },
      "start_date": "2012-01-01",
      "id": "10201025215846844"
    },
    {
      "end_date": "2011-01-01",
      "employer": {
        "id": "436315863143392",
        "name": "CXC Global"
      },
      "position": {
        "id": "103146756409401",
        "name": "PHP Developer"
      },
      "start_date": "2008-01-01",
      "id": "10201025215766842"
    }
  ],
  "education": [
    {
      "school": {
        "id": "271159269566195",
        "name": "Southeastern College - (SEC)"
      },
      "type": "High School",
      "year": {
        "id": "140829239279495",
        "name": "1992"
      },
      "id": "10200538063628343"
    },
    {
      "concentration": [
        {
          "id": "200834693264812",
          "name": "BS Computer Engineering"
        }
      ],
      "school": {
        "id": "103778899660355",
        "name": "Adamson University"
      },
      "type": "College",
      "year": {
        "id": "131821060195210",
        "name": "1997"
      },
      "id": "2630554837388"
    },
    {
      "concentration": [
        {
          "id": "200834693264812",
          "name": "BS Computer Engineering"
        }
      ],
      "school": {
        "id": "108134942553157",
        "name": "Aquinas University of Legazpi"
      },
      "type": "College",
      "year": {
        "id": "143018465715205",
        "name": "2000"
      },
      "id": "1777083901148"
    },
    {
      "school": {
        "id": "320785304623291",
        "name": "Polytechnic University of the Philippines Open University"
      },
      "type": "Graduate School",
      "id": "10201025187286130"
    },
    {
      "concentration": [
        {
          "id": "260449050731702",
          "name": "Master of Science In Information Technology"
        }
      ],
      "school": {
        "id": "320785304623291",
        "name": "Polytechnic University of the Philippines Open University"
      },
      "type": "Graduate School",
      "id": "10201025168565662"
    }
  ]
}';
$newres = json_decode($response, true);
//print_r($newres);
echo '</pre>';
?>
<img src="<?php echo $newres['picture']['data']['url']; ?>" alt=""/><br>
Email: <?php echo $newres['email']; ?><br>
Work Experience\s:<br>---<br>
<?php
$work = '';
foreach ($newres['work'] as &$value) {
    $work .= 'Employer: '.$value['employer']['name'].'<br>';
    $work .= 'Location: '.$value['location']['name'].'<br>';
    $work .= '---<br>';
}
echo $work;
?>
Education:<br>---<br>
<?php
$educ = '';
foreach ($newres['education'] as &$value) {
    $educ .= 'School: '.$value['school']['name'].'<br>';
    $educ .= 'Type: '.$value['type'].'<br>';
    $educ .= 'Concentration: '.$value['concentration']['name'].'<br>';
    $educ .= '---<br>';
}
echo $educ;