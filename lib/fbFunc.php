<?php

function getUserProfileDetails($nodeId)
{
    $fb = new Facebook\Facebook([
        'app_id' => '912753012162007',
        'app_secret' => '564e9ad115ecc2f1deb0cfc417f5b9bc',
        'default_graph_version' => 'v2.7',
    ]);

    try
    {
        // Returns a `Facebook\FacebookResponse` object
        $response = $fb->get('/me?fields=id,name', '{access-token}');
    }
    catch(Facebook\Exceptions\FacebookResponseException $e)
    {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    }
    catch(Facebook\Exceptions\FacebookSDKException $e)
    {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    return $response;
}

